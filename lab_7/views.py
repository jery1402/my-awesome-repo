from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from django.core import serializers

from django.db import IntegrityError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {'author' : 'Jeremy Valentino Manik'}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()
    paginator = Paginator(mahasiswa_list, 7)

    page = request.GET.get('page', 1)
    mahasiswa_list = paginator.page(page)

    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    response['author'] = 'Jeremy Valentino Manik'
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)
    
def get_friend_list(request):
	if request.method == 'GET':
		friend_list = Friend.objects.all()
		data = serializers.serialize('json', friend_list)
		return HttpResponse(data)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        
        try:
            address = request.POST['alamat']
            postal_code = request.POST['kode_pos']
            birth_place = request.POST['kota_lahir']
            birth_date = request.POST['tgl_lahir']
        
            generation = request.POST['angkatan']
            study_field = request.POST['nm_org']
            study_program = request.POST['nm_prg']
            friend = Friend(
                friend_name=name,
                npm=npm,
                
                address=address,
                postal_code=postal_code,
                birth_date=birth_date,
                birth_place=birth_place,
                
                generation=generation,
                study_field=study_field,
                study_program=study_program)
        except:
            friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

@csrf_exempt
def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
    
def friend_detail(request, friend_id):
    friend = Friend.objects.get(id=friend_id)
    response['friend'] = friend
    html = 'lab_7/friend_detail.html'
    return render(request, html, response)
